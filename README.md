# Welcome

DBtrends is about easy browsing features, knowledge summarization and ranking.
There are several ranking and index algorithms available. In DBtrends we try to put them together in a lightweight library, so you can easily use them in your projects.
DBtrends do not run on service, it offers useful methods in an easy and self contained library.
There is no need for a database, third party libraries or configuration.
Download, plug & play.
You can read more about how to use the library consulting the [API](https://bitbucket.org/emarx/dbtrends/wiki/Home) documentation.


####*** Using DBtrends on your project ***

1) Add the following dependency on your project:

```
<dependencies>
  <dependency>
    <groupId>org.dbtrends</groupId>
    <artifactId>dbtrends.core</artifactId>
    <version>0.1.3-beta2</version>
  </dependency>
...
</dependencies>
```

2) Add the internal AKSW repository on your pom file:

```
<repositories>
    <repository>
      <id>maven.aksw.internal</id>
      <name>University Leipzig, AKSW Maven2 Repository</name>
      <url>http://maven.aksw.org/archiva/repository/internal</url>
    </repository>
  ...
</repositories>
```

3) Rock it.. ;-)

Check the [Library Documentation](https://bitbucket.org/emarx/dbtrends/wiki/Home) for more.

To cite this work please use:

```
@inproceedings{dbtrends2016,  
author = {Marx, Edgard and Zaveri, Amrapali and Mohammed, Mofeed and Rautenberg, Sandro and Lehmann, Jens and Ngomo, Axel-Cyrille Ngonga and Cheng, Gong},
  booktitle = {13th Extended Semantic Web Conference (ESWC 2016), 2nd International Workshop on Summarizing and Presenting Entities and Ontologies},
  series = {SUMPRE'16}, 
  title = {{DBtrends} : {P}ublishing and {B}enchmarking {RDF} {R}anking {F}unctions},
  year = 2016
}
```


## NEWS ##

### 24/04/2017

#### DBtrends 0.1.3.beta2 Released, fully operating on Maven:


```
<dependencies>
  <dependency>
    <groupId>org.dbtrends</groupId>
    <artifactId>dbtrends.core</artifactId>
    <version>0.1.3-beta2</version>
  </dependency>
...
</dependencies>
```


### 24/05/2015
     
#### DBtrends 0.1.2b Released [download](https://www.dropbox.com/s/3xyerys4co0406p/dbtrends.core-0.1.2b.jar)

##### Features

- ###### API enhancement

### 01/05/2015
     
#### DBtrends 0.1.1b Released [download](https://bitbucket.org/emarx/dbtrends/downloads/dbtrends.core-0.1.1b.jar)

##### BUGS

- ###### Fix memory leaks

### 07/04/2015
     
#### DBtrends 0.1b Released [download](https://bitbucket.org/emarx/dbtrends/downloads/dbtrends.core_0.1b.jar)